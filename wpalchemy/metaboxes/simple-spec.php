<?php

$custom_metabox = $simple_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_ribbon_link',
	'title' => 'Ribbon Link',
	'template' => RIBBON_SHOP_PATH . 'wpalchemy/metaboxes/simple-meta.php',
	'types' => array( 'ribbon_product' ),
	'context' => 'normal',
	'priority' => 'high',
	'autosave' => true,
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'save_action' => 'my_save_action_func'
));

function my_save_action_func( $meta, $post_id ) {
	$post = get_post( $post_id );
	if( empty( $post->post_title ) ) {
		wp_update_post( array(
			'ID' => $post_id,
			'post_title' => basename( $meta['ribbon_link'] ),
		));
	}
}