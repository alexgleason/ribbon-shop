<?php

include_once RIBBON_SHOP_PATH . 'wpalchemy/MetaBox.php';

// global styles for the meta boxes
if (is_admin()) add_action('admin_enqueue_scripts', 'metabox_style');

function metabox_style() {
	wp_enqueue_style('wpalchemy-metabox', RIBBON_SHOP_URL . 'wpalchemy/metaboxes/meta.css');
}