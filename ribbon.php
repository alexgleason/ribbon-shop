<?php
/**
 * @package Ribbon
 * @version 1.0
 */
/*
Plugin Name: Ribbon Shop
Plugin URI: http://alexgleason.me/
Description: Unofficial Ribbon Shop for Wordpress
Author: Alex Gleason
Version: 1.0
Author URI: http://alexgleason.me/
*/

define( 'RIBBON_SHOP_PATH', plugin_dir_path( __FILE__ ) );
define( 'RIBBON_SHOP_URL', plugins_url( '', __FILE__ ) . '/' );

include_once RIBBON_SHOP_PATH . 'wpalchemy/metaboxes/setup.php';
include_once RIBBON_SHOP_PATH . 'wpalchemy/metaboxes/simple-spec.php';

add_action( 'init', 'create_ribbon_post_type' );
add_action( 'admin_init', 'enqueue_stuff' );
add_action( 'wp_enqueue_scripts', 'enqueue_stuff' );
add_action( 'widgets_init', 'register_ribbon_widget' );

add_shortcode( 'ribbon', 'ribbon_shortcode' );

function create_ribbon_post_type() {
	register_post_type( 'ribbon_product',
		array(
			'labels' => array(
				'name' => __( 'Ribbon Products' ),
				'singular_name' => __( 'Ribbon Product' ),
				'all_items' => __( 'All Products' )
			),
			'supports' => array(
				'editor' => false,
			),
			'public' => true,
			'has_archive' => true,
		)
	);
	add_post_type_support( 'ribbon_product', 'title' );
}

function enqueue_stuff() {
	wp_enqueue_style( 'ribbon', RIBBON_SHOP_URL . 'ribbon.css' );
}

function ribbon_shortcode( $atts ){
	$my_query = new WP_Query(
		array(
			'post_type' => 'ribbon_product',
			'post_status' => 'publish',
			'posts_per_page' => -1
		)
	);

	if( $my_query->have_posts() ) {
		while ($my_query->have_posts()) : $my_query->the_post(); ?>
		<div class="ribbon-product">
			<iframe src="http://rbn.co/<?php echo basename( get_post_meta( get_the_ID(), 'ribbon_link', true ) ); ?>?embed=true"></iframe>
		</div>
		<?php endwhile;
	}
}

function register_ribbon_widget() {
	register_widget( 'Ribbon_Widget' );
}

class Ribbon_Widget extends WP_Widget {
	function Ribbon_Widget() {
		$widget_ops = array( 'classname' => 'ribbon-widget', 'description' => __('A widget that displays a Ribbon shopping item', 'ribbon-widget') );
		$control_ops = array( 'id_base' => 'ribbon-widget' );
		$this->WP_Widget( 'ribbon-widget', __('Ribbon Widget', 'ribbon-widget'), $widget_ops, $control_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );
		$ribbon_link = $instance['ribbon_link'];
		echo $before_widget;
		?>
		<div class="ribbon-product">
			<iframe src="http://rbn.co/<?php echo basename( $ribbon_link ); ?>?embed=true"></iframe>
		</div>
		<?php
		echo $after_widget;
	}
	function update( $new_instance, $instance ) {  
		$instance['ribbon_link'] = $new_instance['ribbon_link'];
		return $instance;
	}
	function form( $instance ) {
		?>
		<p>
			<label for="<?php echo $this->get_field_id('ribbon_link'); ?>"><?php _e('Ribbon Link'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('ribbon_link'); ?>" name="<?php echo $this->get_field_name('ribbon_link'); ?>" type="text" value="<?php echo $instance['ribbon_link']; ?>" />
		</p>
		<?php
	}
}
